///
/// File: pipeline.c
///
/// Process arguments from the command line and 
/// pipes them together
///
/// @author Kevin Allison

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

int search_start;		// The position to start searching at in get_next_command

/// Structure to hold information about a given command
///
/// @field	cmd			The argument array to pass to exec
///							0 -> command name
///							1 -> first argument
///							...
///							n-1 -> last argument
///							n -> null terminator
///
/// @field	in_red		The file to redirect standard input to if this
///						is the first command, NULL otherwise
///
/// @field	out_red		The file to redirect standard output to if this is
///						the last command, NULL otherwise
///
/// @field	has_next	1 if this is not the last command in the sequence
///						0 if this is the last command in the sequence
///
/// @field	has_prev	1 if this is not the first command in the sequence
/// 					0 if this is the first command in the sequence
typedef struct {
	char **cmd;
	char *in_red;
	char *out_red;
	int has_next;
	int has_prev;
} cmd_info_t;

/// Parse out the next command based on the starting position stored
/// in search_start.  Parsing starts at the end of argv and continues
/// until it hits a pipe or the position becomes less than 1
///
/// @param	argc		The number of entries in argv
/// @param	argv		The arguments read in from the command line
///
/// @return 			A structure containing a valid char** to pass to
///						exec functions and information about the surrounding commands
cmd_info_t get_next_command(int argc, char **argv) {

	cmd_info_t info;
	if (search_start < 1) { info.cmd = NULL; return info; }

	// Set the input/output redirects to NULL
	// Only commands allowed to have one are the first and last commands
	info.in_red = NULL;
	info.out_red  = NULL;

	// If the starting location for the search plus one is greater than argc
	// then is the last command in the sequence
	info.has_next = (search_start + 1 < argc) ? 1 : 0;

	int stop;		// Store the current position when iterating over argv
	// Search backwards until a pipe is found
	for (stop = search_start; strcmp(argv[stop], "|") != 0 && stop > 0; stop--)
		;
	stop++;			// Increment pos to set it to the command
	info.has_prev = (stop - 1 < 1) ? 0 : 1;

	// If the first command is a pipe or two pipes are next to each other
	if (strcmp(argv[1], "|") == 0 || search_start - stop < 0) { 
		fprintf(stderr, "Illegal null command\n");
		exit(EXIT_FAILURE);
	}

	info.cmd = &argv[stop];
	int num_args = search_start - stop + 1;

	if (search_start + 1 < argc) {
		// Set the position after the last argument to the NULL pointer
		info.cmd[num_args] = (char*)NULL;
	}

	int num_out_redir = 0;
	int num_in_redir = 0;
	for (int pos = 0; pos < num_args; pos++) {
		if (strcmp(info.cmd[pos], ">") == 0 || strcmp(info.cmd[pos], "<") == 0) {
			// Found Input/Output Redirecter
			strcmp(info.cmd[pos], ">") == 0 ? num_out_redir++ : num_in_redir++;

			// Check to see if we should have another command after this one
			if (strcmp(info.cmd[pos], ">") == 0 && num_out_redir > 1) {
				fprintf(stderr, "%s: Illegal output redirect\n", info.cmd[pos + 1]);
				exit(EXIT_FAILURE);
			}

			// Check for missing command name
			if (pos == 0 || strcmp(info.cmd[pos - 1], "|") == 0) {
				fprintf(stderr, "Missing command name\n");
				exit(EXIT_FAILURE);
			}

			if (pos + 1 >= num_args || strcmp(info.cmd[pos + 1], "|") == 0) {
				fprintf(stderr, "Missing redirect filename\n");
				exit(EXIT_FAILURE);
			} else if (((pos + 2 < num_args && (strcmp(info.cmd[pos + 2], "|") != 0 && info.has_next)) ||
					   (!info.has_next && pos + 2 < num_args)) &&
					   (strcmp(info.cmd[pos + 2], "<") != 0 && strcmp(info.cmd[pos + 2], ">") != 0)) {
				fprintf(stderr, "%s: Illegal argument after redirect\n", info.cmd[pos + 2]);
				exit(EXIT_FAILURE);
			}

			if (strcmp(info.cmd[pos], ">") == 0) {
				if (info.has_next || num_out_redir > 1) {
					fprintf(stderr, "%s: Illegal output redirect\n", info.cmd[pos+1]);
					exit(EXIT_FAILURE);
				} else {
					info.out_red = info.cmd[pos + 1];
				}
			}

			if (strcmp(info.cmd[pos], "<") == 0) {
				if (info.has_prev || num_in_redir > 1) {
					fprintf(stderr, "%s: Illegal input redirect\n", info.cmd[pos+1]);
					exit(EXIT_FAILURE);
				} else {
					info.in_red = info.cmd[pos + 1];
				}
			}

			// Change the info stored here to point to null to avoid
			// sending it to exec and getting errors
			info.cmd[pos] = (char*)NULL;
		}
	}

	// Decrement the starting location by 2 to start just past the pipe
	search_start = stop - 2;
	return info;
}

int main(int argc, char **argv) {

	search_start = argc - 1;	// Set the contextual info for search for commands

	cmd_info_t info;
	int next_fd[2];
	int prev_fd[2];

	while ((info = get_next_command(argc, argv)).cmd != NULL) {
		// Open pipe between this and previous command
		int ret = pipe(prev_fd);
		if (ret == -1) { perror("pipe"); exit(EXIT_FAILURE); }
		switch (fork()) {
			case -1:
				perror("fork");
				exit(EXIT_FAILURE);
			case 0:
				// Execute the new command
				if (info.has_prev) {
					// If there is a previous command
					// set up stdin to read from a pipe
					close(prev_fd[1]);
					dup2(prev_fd[0], STDIN_FILENO);
					close(prev_fd[0]);
				} else if (info.in_red != NULL) {
					// If there is a file to read in from
					// duplicate that to standard in
					int in = open(info.in_red, O_RDONLY);
					if (in == -1) {
						perror(info.in_red);
						exit(EXIT_FAILURE);
					}
					dup2(in, STDIN_FILENO);
					close(in);
				}

				if (info.has_next) {
					// If there is another command after this one
					// set up stdout to write to a pipe
					close(next_fd[0]);
					dup2(next_fd[1], STDOUT_FILENO);
					close(next_fd[1]);
				} else if (info.out_red != NULL) {
					// If there is a file to output to 
					// redirect standard out to a file
					int out = open(info.out_red, O_CREAT|O_WRONLY, S_IRWXU|S_IRGRP|S_IROTH); 
					if (out == -1) {
						perror(info.out_red);
						exit(EXIT_FAILURE);
					}
					dup2(out, STDOUT_FILENO);
					close(out);
				}
		
				execvp(info.cmd[0], info.cmd);
				/** NEVER REACHED **/
				break;
			default:
				if (info.has_prev) {
					// Set the prev file descriptors to be the next 
					// ones since we are traversing the list in reverse
					next_fd[0] = prev_fd[0];
					next_fd[1] = prev_fd[1];
				}
		}
	}

	// Wait for first child (last command) to terminate
	wait(NULL);

	return EXIT_SUCCESS;
}
